package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Slot;
import com.mycompany.myapp.repository.SlotRepository;
import com.mycompany.myapp.repository.search.SlotSearchRepository;
import com.mycompany.myapp.service.dto.SlotDTO;
import com.mycompany.myapp.service.mapper.SlotMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Slot.
 */
@Service
@Transactional
public class SlotService {

    private final Logger log = LoggerFactory.getLogger(SlotService.class);

    private final SlotRepository slotRepository;

    private final SlotMapper slotMapper;

    private final SlotSearchRepository slotSearchRepository;

    public SlotService(SlotRepository slotRepository, SlotMapper slotMapper, SlotSearchRepository slotSearchRepository) {
        this.slotRepository = slotRepository;
        this.slotMapper = slotMapper;
        this.slotSearchRepository = slotSearchRepository;
    }

    /**
     * Save a slot.
     *
     * @param slotDTO the entity to save
     * @return the persisted entity
     */
    public SlotDTO save(SlotDTO slotDTO) {
        log.debug("Request to save Slot : {}", slotDTO);
        Slot slot = slotMapper.toEntity(slotDTO);
        slot = slotRepository.save(slot);
        SlotDTO result = slotMapper.toDto(slot);
        slotSearchRepository.save(slot);
        return result;
    }

    /**
     *  Get all the slots.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SlotDTO> findAll() {
        log.debug("Request to get all Slots");
        return slotRepository.findAll().stream()
            .map(slotMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one slot by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SlotDTO findOne(Long id) {
        log.debug("Request to get Slot : {}", id);
        Slot slot = slotRepository.findOne(id);
        return slotMapper.toDto(slot);
    }

    /**
     *  Delete the  slot by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Slot : {}", id);
        slotRepository.delete(id);
        slotSearchRepository.delete(id);
    }

    /**
     * Search for the slot corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SlotDTO> search(String query) {
        log.debug("Request to search Slots for query {}", query);
        return StreamSupport
            .stream(slotSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(slotMapper::toDto)
            .collect(Collectors.toList());
    }
}
