package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.SubjectResource;
import com.mycompany.myapp.repository.SubjectResourceRepository;
import com.mycompany.myapp.repository.search.SubjectResourceSearchRepository;
import com.mycompany.myapp.service.dto.SubjectResourceDTO;
import com.mycompany.myapp.service.mapper.SubjectResourceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SubjectResource.
 */
@Service
@Transactional
public class SubjectResourceService {

    private final Logger log = LoggerFactory.getLogger(SubjectResourceService.class);

    private final SubjectResourceRepository subjectResourceRepository;

    private final SubjectResourceMapper subjectResourceMapper;

    private final SubjectResourceSearchRepository subjectResourceSearchRepository;

    public SubjectResourceService(SubjectResourceRepository subjectResourceRepository, SubjectResourceMapper subjectResourceMapper, SubjectResourceSearchRepository subjectResourceSearchRepository) {
        this.subjectResourceRepository = subjectResourceRepository;
        this.subjectResourceMapper = subjectResourceMapper;
        this.subjectResourceSearchRepository = subjectResourceSearchRepository;
    }

    /**
     * Save a subjectResource.
     *
     * @param subjectResourceDTO the entity to save
     * @return the persisted entity
     */
    public SubjectResourceDTO save(SubjectResourceDTO subjectResourceDTO) {
        log.debug("Request to save SubjectResource : {}", subjectResourceDTO);
        SubjectResource subjectResource = subjectResourceMapper.toEntity(subjectResourceDTO);
        subjectResource = subjectResourceRepository.save(subjectResource);
        SubjectResourceDTO result = subjectResourceMapper.toDto(subjectResource);
        subjectResourceSearchRepository.save(subjectResource);
        return result;
    }

    /**
     *  Get all the subjectResources.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SubjectResourceDTO> findAll() {
        log.debug("Request to get all SubjectResources");
        return subjectResourceRepository.findAll().stream()
            .map(subjectResourceMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one subjectResource by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SubjectResourceDTO findOne(Long id) {
        log.debug("Request to get SubjectResource : {}", id);
        SubjectResource subjectResource = subjectResourceRepository.findOne(id);
        return subjectResourceMapper.toDto(subjectResource);
    }

    /**
     *  Delete the  subjectResource by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SubjectResource : {}", id);
        subjectResourceRepository.delete(id);
        subjectResourceSearchRepository.delete(id);
    }

    /**
     * Search for the subjectResource corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SubjectResourceDTO> search(String query) {
        log.debug("Request to search SubjectResources for query {}", query);
        return StreamSupport
            .stream(subjectResourceSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(subjectResourceMapper::toDto)
            .collect(Collectors.toList());
    }
}
