package com.mycompany.myapp.service.dto;

import com.mycompany.myapp.domain.SubjectResource;

import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

public class AlgorithmCourseDTO {

    @NotNull
    private Integer studentSize;

    @NotNull
    private Set<AlgorithmSubjectResourceDTO> subjects;

    public AlgorithmCourseDTO() {
        subjects = new HashSet<>();
    }

    public Integer getStudentSize() {
        return studentSize;
    }

    public void setStudentSize(Integer studentSize) {
        this.studentSize = studentSize;
    }

    public Set<AlgorithmSubjectResourceDTO> getSubjects() {
        return subjects;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AlgorithmCourseDTO that = (AlgorithmCourseDTO) o;

        if (!studentSize.equals(that.studentSize)) return false;
        return subjects.equals(that.subjects);
    }

    @Override
    public int hashCode() {
        int result = studentSize.hashCode();
        result = 31 * result + subjects.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "AlgorithmCourseDTO{" +
            "studentSize=" + studentSize +
            ", subjects=" + subjects +
            '}';
    }
}

