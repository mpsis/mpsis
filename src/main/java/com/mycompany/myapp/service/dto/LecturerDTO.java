package com.mycompany.myapp.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Lecturer entity.
 */
public class LecturerDTO implements Serializable {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LecturerDTO lecturerDTO = (LecturerDTO) o;
        if(lecturerDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), lecturerDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LecturerDTO{" +
            "id=" + getId() +
            "}";
    }
}
