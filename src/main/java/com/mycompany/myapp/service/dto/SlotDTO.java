package com.mycompany.myapp.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.mycompany.myapp.domain.enumeration.Day;

/**
 * A DTO for the Slot entity.
 */
public class SlotDTO implements Serializable {

    private Long id;

    @NotNull
    private Day day;

    @NotNull
    @Min(value = 1)
    @Max(value = 16)
    private Integer number;

    private Long roomId;

    private Long subjectId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectResourceId) {
        this.subjectId = subjectResourceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SlotDTO slotDTO = (SlotDTO) o;
        if(slotDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), slotDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SlotDTO{" +
            "id=" + getId() +
            ", day='" + getDay() + "'" +
            ", number='" + getNumber() + "'" +
            "}";
    }
}
