package com.mycompany.myapp.service.dto;

import com.mycompany.myapp.domain.Room;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class AlgorithmBuildingDto {

    private final Set<AlgorithmRoomDto> rooms;

    public AlgorithmBuildingDto(Set<AlgorithmRoomDto> rooms) {
        this.rooms = Collections.unmodifiableSet(rooms);
    }

    public Set<AlgorithmRoomDto> getRooms() {
        return rooms;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AlgorithmBuildingDto that = (AlgorithmBuildingDto) o;

        return rooms.equals(that.rooms);
    }

    @Override
    public int hashCode() {
        return rooms.hashCode();
    }

    @Override
    public String toString() {
        return "AlgorithmBuildingDto{" +
            "rooms=" + rooms +
            '}';
    }
}
