package com.mycompany.myapp.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.mycompany.myapp.domain.enumeration.SubjectType;

/**
 * A DTO for the SubjectResource entity.
 */
public class SubjectResourceDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer duration;

    @NotNull
    private SubjectType type;

    private Long subjectId;

    private String subjectName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public SubjectType getType() {
        return type;
    }

    public void setType(SubjectType type) {
        this.type = type;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SubjectResourceDTO subjectResourceDTO = (SubjectResourceDTO) o;
        if(subjectResourceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), subjectResourceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SubjectResourceDTO{" +
            "id=" + getId() +
            ", duration='" + getDuration() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
