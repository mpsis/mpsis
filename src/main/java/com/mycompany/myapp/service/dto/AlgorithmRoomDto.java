package com.mycompany.myapp.service.dto;

import com.mycompany.myapp.domain.Slot;
import com.mycompany.myapp.domain.enumeration.SubjectType;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Set;

public class AlgorithmRoomDto {

    @NotNull
    private final Long id;

    @NotNull
    private final Integer capacity;

    @NotNull
    private final SubjectType type;

    private final Set<SlotDTO> slots;

    public AlgorithmRoomDto(Long id, Integer capacity, SubjectType type, Set<SlotDTO> slots) {
        this.id = id;
        this.capacity = capacity;
        this.type = type;
        this.slots = Collections.unmodifiableSet(slots);
    }

    public Long getId() {
        return id;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public SubjectType getType() {
        return type;
    }

    public Set<SlotDTO> getSlots() {
        return slots;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AlgorithmRoomDto that = (AlgorithmRoomDto) o;

        if (!id.equals(that.id)) return false;
        if (!capacity.equals(that.capacity)) return false;
        if (type != that.type) return false;
        return slots.equals(that.slots);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + capacity.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + slots.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "AlgorithmRoomDto{" +
            "id=" + id +
            ", capacity=" + capacity +
            ", type=" + type +
            ", slots=" + slots +
            '}';
    }
}
