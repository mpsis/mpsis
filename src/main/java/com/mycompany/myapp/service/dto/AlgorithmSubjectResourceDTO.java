package com.mycompany.myapp.service.dto;

import com.mycompany.myapp.domain.enumeration.SubjectType;

import javax.validation.constraints.NotNull;

public class AlgorithmSubjectResourceDTO {

    @NotNull
    private Long id;

    @NotNull
    private Integer duration;

    @NotNull
    private Long lecturer;

    @NotNull
    private SubjectType type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Long getLecturer() {
        return lecturer;
    }

    public void setLecturer(Long lecturer) {
        this.lecturer = lecturer;
    }

    public SubjectType getType() {
        return type;
    }

    public void setType(SubjectType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AlgorithmSubjectResourceDTO that = (AlgorithmSubjectResourceDTO) o;

        if (!id.equals(that.id)) return false;
        if (!duration.equals(that.duration)) return false;
        if (!lecturer.equals(that.lecturer)) return false;
        return type == that.type;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + duration.hashCode();
        result = 31 * result + lecturer.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "AlgorithmSubjectResourceDTO{" +
            "id=" + id +
            ", duration=" + duration +
            ", lecturer=" + lecturer +
            ", type=" + type +
            '}';
    }
}
