package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Building;
import com.mycompany.myapp.domain.Course;
import com.mycompany.myapp.repository.BuildingRepository;
import com.mycompany.myapp.repository.CourseRepository;
import com.mycompany.myapp.service.dto.AlgorithmBuildingDto;
import com.mycompany.myapp.service.dto.AlgorithmCourseDTO;
import com.mycompany.myapp.service.mapper.AlgorithmBuildingMapper;
import com.mycompany.myapp.service.mapper.AlgorithmCourseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AlgorithmService {

    private final Logger log = LoggerFactory.getLogger(AlgorithmService.class);

    private final CourseRepository courseRepository;
    private final BuildingRepository buildingRepository;

    private final AlgorithmCourseMapper algorithmCourseMapper;
    private final AlgorithmBuildingMapper algorithmBuildingMapper;

    public AlgorithmService(CourseRepository courseRepository, BuildingRepository buildingRepository, AlgorithmCourseMapper algorithmCourseMapper, AlgorithmBuildingMapper algorithmBuildingMapper) {
        this.courseRepository = courseRepository;
        this.buildingRepository = buildingRepository;
        this.algorithmCourseMapper = algorithmCourseMapper;
        this.algorithmBuildingMapper = algorithmBuildingMapper;
    }

    @Transactional(readOnly = true)
    public AlgorithmCourseDTO getAlgorithmCourseResource(Long id){
        log.debug("Request to get AlgorithmCourse : {}", id);
        Course course = courseRepository.findOne(id);
        return algorithmCourseMapper.courseToAlgorithmCourseDTO(course);
    }

    @Transactional(readOnly = true)
    public AlgorithmBuildingDto getAlgorithmBuildingResource(Long id){
        log.debug("Request to get AlgorithmBuilding : {}", id);
        Building building = buildingRepository.findOne(id);
        return algorithmBuildingMapper.buildingToAlgorithmBuildingDto(building);
    }

}
