package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Building;
import com.mycompany.myapp.service.dto.AlgorithmBuildingDto;
import com.mycompany.myapp.service.dto.AlgorithmRoomDto;
import com.mycompany.myapp.service.dto.SlotDTO;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class AlgorithmBuildingMapper {

    private final SlotMapper slotMapper;

    public AlgorithmBuildingMapper(SlotMapper slotMapper) {
        this.slotMapper = slotMapper;
    }

    public AlgorithmBuildingDto buildingToAlgorithmBuildingDto(Building building) {
        Set<AlgorithmRoomDto> roomsDto = new HashSet<>();
        building.getRooms().forEach(room -> {
            Set<SlotDTO> slotsDto = new HashSet<>();
            room.getSlots().stream().forEach(slot -> slotsDto.add(slotMapper.toDto(slot)));
            roomsDto.add(new AlgorithmRoomDto(room.getId(),
                room.getCapacity(),room.getType(),slotsDto));
        });
        return new AlgorithmBuildingDto(roomsDto);
    }
}
