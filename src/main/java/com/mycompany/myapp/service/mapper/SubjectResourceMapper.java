package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.SubjectResourceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SubjectResource and its DTO SubjectResourceDTO.
 */
@Mapper(componentModel = "spring", uses = {SubjectMapper.class})
public interface SubjectResourceMapper extends EntityMapper<SubjectResourceDTO, SubjectResource> {

    @Mapping(source = "subject.id", target = "subjectId")
    @Mapping(source = "subject.name", target = "subjectName")
    SubjectResourceDTO toDto(SubjectResource subjectResource); 

    @Mapping(target = "slots", ignore = true)
    @Mapping(source = "subjectId", target = "subject")
    SubjectResource toEntity(SubjectResourceDTO subjectResourceDTO);

    default SubjectResource fromId(Long id) {
        if (id == null) {
            return null;
        }
        SubjectResource subjectResource = new SubjectResource();
        subjectResource.setId(id);
        return subjectResource;
    }
}
