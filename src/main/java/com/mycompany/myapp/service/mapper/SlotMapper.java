package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.SlotDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Slot and its DTO SlotDTO.
 */
@Mapper(componentModel = "spring", uses = {RoomMapper.class, SubjectResourceMapper.class})
public interface SlotMapper extends EntityMapper<SlotDTO, Slot> {

    @Mapping(source = "room.id", target = "roomId")
    @Mapping(source = "subject.id", target = "subjectId")
    SlotDTO toDto(Slot slot); 

    @Mapping(source = "roomId", target = "room")
    @Mapping(source = "subjectId", target = "subject")
    Slot toEntity(SlotDTO slotDTO);

    default Slot fromId(Long id) {
        if (id == null) {
            return null;
        }
        Slot slot = new Slot();
        slot.setId(id);
        return slot;
    }
}
