package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Course;
import com.mycompany.myapp.domain.Subject;
import com.mycompany.myapp.domain.SubjectResource;
import com.mycompany.myapp.service.dto.AlgorithmCourseDTO;
import com.mycompany.myapp.service.dto.AlgorithmSubjectResourceDTO;
import org.springframework.stereotype.Service;

@Service
public class AlgorithmCourseMapper {

    public AlgorithmCourseDTO courseToAlgorithmCourseDTO(Course course) {
        if(course == null){
            return null;
        }
        AlgorithmCourseDTO dto = new AlgorithmCourseDTO();
        dto.setStudentSize(course.getStudents().size());
        for(Subject subject: course.getSubjects()) {
            for(SubjectResource subjectResource: subject.getSubjectResources()){
                AlgorithmSubjectResourceDTO resDto = new AlgorithmSubjectResourceDTO();
                resDto.setLecturer(subject.getLecturer().getId());
                resDto.setDuration(subjectResource.getDuration());
                resDto.setType(subjectResource.getType());
                resDto.setId(subjectResource.getId());
                dto.getSubjects().add(resDto);
            }
        }
        return dto;
    }
}
