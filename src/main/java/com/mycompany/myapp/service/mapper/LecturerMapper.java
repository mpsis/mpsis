package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.LecturerDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Lecturer and its DTO LecturerDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LecturerMapper extends EntityMapper<LecturerDTO, Lecturer> {

    

    @Mapping(target = "subjects", ignore = true)
    Lecturer toEntity(LecturerDTO lecturerDTO);

    default Lecturer fromId(Long id) {
        if (id == null) {
            return null;
        }
        Lecturer lecturer = new Lecturer();
        lecturer.setId(id);
        return lecturer;
    }
}
