package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Room;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;


/**
 * Spring Data JPA repository for the Room entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {

    Optional<List<Room>> findAllByBuilding_Id(Long id);

}
