package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.SubjectResource;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SubjectResource entity.
 */
public interface SubjectResourceSearchRepository extends ElasticsearchRepository<SubjectResource, Long> {
}
