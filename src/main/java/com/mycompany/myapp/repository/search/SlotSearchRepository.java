package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.Slot;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Slot entity.
 */
public interface SlotSearchRepository extends ElasticsearchRepository<Slot, Long> {
}
