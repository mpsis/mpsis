package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.Building;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Building entity.
 */
public interface BuildingSearchRepository extends ElasticsearchRepository<Building, Long> {
}
