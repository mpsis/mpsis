package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.Lecturer;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Lecturer entity.
 */
public interface LecturerSearchRepository extends ElasticsearchRepository<Lecturer, Long> {
}
