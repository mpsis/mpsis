package com.mycompany.myapp.domain.enumeration;

/**
 * The SubjectType enumeration.
 */
public enum SubjectType {
    LECTURE, LABOLATORY, EXERCISE
}
