package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.mycompany.myapp.domain.enumeration.SubjectType;

/**
 * A SubjectResource.
 */
@Entity
@Table(name = "subject_resource")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "subjectresource")
public class SubjectResource implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "duration", nullable = false)
    private Integer duration;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type", nullable = false)
    private SubjectType type;

    @OneToMany(mappedBy = "subject")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Slot> slots = new HashSet<>();

    @ManyToOne
    private Subject subject;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDuration() {
        return duration;
    }

    public SubjectResource duration(Integer duration) {
        this.duration = duration;
        return this;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public SubjectType getType() {
        return type;
    }

    public SubjectResource type(SubjectType type) {
        this.type = type;
        return this;
    }

    public void setType(SubjectType type) {
        this.type = type;
    }

    public Set<Slot> getSlots() {
        return slots;
    }

    public SubjectResource slots(Set<Slot> slots) {
        this.slots = slots;
        return this;
    }

    public SubjectResource addSlot(Slot slot) {
        this.slots.add(slot);
        slot.setSubject(this);
        return this;
    }

    public SubjectResource removeSlot(Slot slot) {
        this.slots.remove(slot);
        slot.setSubject(null);
        return this;
    }

    public void setSlots(Set<Slot> slots) {
        this.slots = slots;
    }

    public Subject getSubject() {
        return subject;
    }

    public SubjectResource subject(Subject subject) {
        this.subject = subject;
        return this;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SubjectResource subjectResource = (SubjectResource) o;
        if (subjectResource.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), subjectResource.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SubjectResource{" +
            "id=" + getId() +
            ", duration='" + getDuration() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
