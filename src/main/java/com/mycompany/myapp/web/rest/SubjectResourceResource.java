package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.SubjectResourceService;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.service.dto.SubjectResourceDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SubjectResource.
 */
@RestController
@RequestMapping("/api")
public class SubjectResourceResource {

    private final Logger log = LoggerFactory.getLogger(SubjectResourceResource.class);

    private static final String ENTITY_NAME = "subjectResource";

    private final SubjectResourceService subjectResourceService;

    public SubjectResourceResource(SubjectResourceService subjectResourceService) {
        this.subjectResourceService = subjectResourceService;
    }

    /**
     * POST  /subject-resources : Create a new subjectResource.
     *
     * @param subjectResourceDTO the subjectResourceDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new subjectResourceDTO, or with status 400 (Bad Request) if the subjectResource has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/subject-resources")
    @Timed
    public ResponseEntity<SubjectResourceDTO> createSubjectResource(@Valid @RequestBody SubjectResourceDTO subjectResourceDTO) throws URISyntaxException {
        log.debug("REST request to save SubjectResource : {}", subjectResourceDTO);
        if (subjectResourceDTO.getId() != null) {
            throw new BadRequestAlertException("A new subjectResource cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SubjectResourceDTO result = subjectResourceService.save(subjectResourceDTO);
        return ResponseEntity.created(new URI("/api/subject-resources/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /subject-resources : Updates an existing subjectResource.
     *
     * @param subjectResourceDTO the subjectResourceDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated subjectResourceDTO,
     * or with status 400 (Bad Request) if the subjectResourceDTO is not valid,
     * or with status 500 (Internal Server Error) if the subjectResourceDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/subject-resources")
    @Timed
    public ResponseEntity<SubjectResourceDTO> updateSubjectResource(@Valid @RequestBody SubjectResourceDTO subjectResourceDTO) throws URISyntaxException {
        log.debug("REST request to update SubjectResource : {}", subjectResourceDTO);
        if (subjectResourceDTO.getId() == null) {
            return createSubjectResource(subjectResourceDTO);
        }
        SubjectResourceDTO result = subjectResourceService.save(subjectResourceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, subjectResourceDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /subject-resources : get all the subjectResources.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of subjectResources in body
     */
    @GetMapping("/subject-resources")
    @Timed
    public List<SubjectResourceDTO> getAllSubjectResources() {
        log.debug("REST request to get all SubjectResources");
        return subjectResourceService.findAll();
        }

    /**
     * GET  /subject-resources/:id : get the "id" subjectResource.
     *
     * @param id the id of the subjectResourceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the subjectResourceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/subject-resources/{id}")
    @Timed
    public ResponseEntity<SubjectResourceDTO> getSubjectResource(@PathVariable Long id) {
        log.debug("REST request to get SubjectResource : {}", id);
        SubjectResourceDTO subjectResourceDTO = subjectResourceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(subjectResourceDTO));
    }

    /**
     * DELETE  /subject-resources/:id : delete the "id" subjectResource.
     *
     * @param id the id of the subjectResourceDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/subject-resources/{id}")
    @Timed
    public ResponseEntity<Void> deleteSubjectResource(@PathVariable Long id) {
        log.debug("REST request to delete SubjectResource : {}", id);
        subjectResourceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/subject-resources?query=:query : search for the subjectResource corresponding
     * to the query.
     *
     * @param query the query of the subjectResource search
     * @return the result of the search
     */
    @GetMapping("/_search/subject-resources")
    @Timed
    public List<SubjectResourceDTO> searchSubjectResources(@RequestParam String query) {
        log.debug("REST request to search SubjectResources for query {}", query);
        return subjectResourceService.search(query);
    }

}
