package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.SlotService;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.service.dto.SlotDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Slot.
 */
@RestController
@RequestMapping("/api")
public class SlotResource {

    private final Logger log = LoggerFactory.getLogger(SlotResource.class);

    private static final String ENTITY_NAME = "slot";

    private final SlotService slotService;

    public SlotResource(SlotService slotService) {
        this.slotService = slotService;
    }

    /**
     * POST  /slots : Create a new slot.
     *
     * @param slotDTO the slotDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new slotDTO, or with status 400 (Bad Request) if the slot has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/slots")
    @Timed
    public ResponseEntity<SlotDTO> createSlot(@Valid @RequestBody SlotDTO slotDTO) throws URISyntaxException {
        log.debug("REST request to save Slot : {}", slotDTO);
        if (slotDTO.getId() != null) {
            throw new BadRequestAlertException("A new slot cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SlotDTO result = slotService.save(slotDTO);
        return ResponseEntity.created(new URI("/api/slots/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /slots : Updates an existing slot.
     *
     * @param slotDTO the slotDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated slotDTO,
     * or with status 400 (Bad Request) if the slotDTO is not valid,
     * or with status 500 (Internal Server Error) if the slotDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/slots")
    @Timed
    public ResponseEntity<SlotDTO> updateSlot(@Valid @RequestBody SlotDTO slotDTO) throws URISyntaxException {
        log.debug("REST request to update Slot : {}", slotDTO);
        if (slotDTO.getId() == null) {
            return createSlot(slotDTO);
        }
        SlotDTO result = slotService.save(slotDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, slotDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /slots : get all the slots.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of slots in body
     */
    @GetMapping("/slots")
    @Timed
    public List<SlotDTO> getAllSlots() {
        log.debug("REST request to get all Slots");
        return slotService.findAll();
        }

    /**
     * GET  /slots/:id : get the "id" slot.
     *
     * @param id the id of the slotDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the slotDTO, or with status 404 (Not Found)
     */
    @GetMapping("/slots/{id}")
    @Timed
    public ResponseEntity<SlotDTO> getSlot(@PathVariable Long id) {
        log.debug("REST request to get Slot : {}", id);
        SlotDTO slotDTO = slotService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(slotDTO));
    }

    /**
     * DELETE  /slots/:id : delete the "id" slot.
     *
     * @param id the id of the slotDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/slots/{id}")
    @Timed
    public ResponseEntity<Void> deleteSlot(@PathVariable Long id) {
        log.debug("REST request to delete Slot : {}", id);
        slotService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/slots?query=:query : search for the slot corresponding
     * to the query.
     *
     * @param query the query of the slot search
     * @return the result of the search
     */
    @GetMapping("/_search/slots")
    @Timed
    public List<SlotDTO> searchSlots(@RequestParam String query) {
        log.debug("REST request to search Slots for query {}", query);
        return slotService.search(query);
    }

}
