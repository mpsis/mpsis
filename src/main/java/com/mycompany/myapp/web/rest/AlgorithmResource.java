package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.AlgorithmService;
import com.mycompany.myapp.service.BuildingService;
import com.mycompany.myapp.service.dto.AlgorithmBuildingDto;
import com.mycompany.myapp.service.dto.AlgorithmCourseDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class AlgorithmResource {
    private final Logger log = LoggerFactory.getLogger(BuildingResource.class);

    private final AlgorithmService algorithmService;

    public AlgorithmResource(AlgorithmService algorithmService) {
        this.algorithmService = algorithmService;
    }

    /**
     * GET  /algorithm/course/{id} : get the "id" of course.
     *
     * @param id the id of the AlgorithmCourseDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the buildingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/algorithm/course/{id}")
    @Timed
    public ResponseEntity<AlgorithmCourseDTO> getAlgorithmCourse(@PathVariable Long id) {
        log.debug("REST request to get a Course resource for algorithm");
        AlgorithmCourseDTO algorithmCourseDTO = algorithmService.getAlgorithmCourseResource(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(algorithmCourseDTO));
    }

    /**
     * GET  /algorithm/building/{id} : get the "id" of building.
     *
     * @param id the id of the AlgorithmCourseDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the buildingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/algorithm/building/{id}")
    @Timed
    public ResponseEntity<AlgorithmBuildingDto> getAlgorithmBuilding(@PathVariable Long id) {
        log.debug("REST request to get a Buliding resource for algorithm");
        AlgorithmBuildingDto buildingDto = algorithmService.getAlgorithmBuildingResource(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(buildingDto));
    }
}
