import { BaseEntity } from './../../shared';

export const enum SubjectType {
    'LECTURE',
    'LABOLATORY',
    'EXERCISE'
}

export class Room implements BaseEntity {
    constructor(
        public id?: number,
        public number?: string,
        public capacity?: number,
        public type?: SubjectType,
        public slots?: BaseEntity[],
        public buildingId?: number,
    ) {
    }
}
