import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Course } from './course.model';
import { SubjectSlot } from './subjectSlot.model';
import { CourseService } from './course.service';

@Component({
    selector: 'jhi-course-detail',
    templateUrl: './course-detail.component.html',
    styleUrls: [
        'course-details.scss'
    ]
})
export class CourseDetailComponent implements OnInit, OnDestroy {

    course: Course;
    timetable: SubjectSlot;
    loader: boolean;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private courseService: CourseService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCourses();
        this.loader = false;
    }

    load(id) {
        this.courseService.find(id).subscribe((course) => {
            this.course = course;
        });
    }
    generateTimetable() {
        this.subscription = this.route.params.subscribe((params) => {
            this.loadTimetable(params['id'])
        });
    }
    loadTimetable(id) {
        this.loader = true;
        this.courseService.getGeneratedSlots(id).subscribe((timetable) => {
            this.timetable = timetable;
            this.loader = false;
        })
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCourses() {
        this.eventSubscriber = this.eventManager.subscribe(
            'courseListModification',
            (response) => this.load(this.course.id)
        );
    }
}
