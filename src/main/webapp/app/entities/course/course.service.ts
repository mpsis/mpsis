import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL, AZURE_URL } from '../../app.constants';
const moment = require('moment')

import { Course } from './course.model';
import { SubjectSlot } from './subjectSlot.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class CourseService {

    private resourceUrl = SERVER_API_URL + 'api/courses';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/courses';

    private slotsUrl = AZURE_URL + 'api/schedules/course/';

    constructor(private http: Http) { }

    getGeneratedSlots(id: number): Observable<object> {
        return this.http.post(this.slotsUrl + id, {}).map((res: Response) => {
            const jsonResponse = res.json();
            console.log('jsonResponse: ', jsonResponse);
            return this.convertTimetableData(jsonResponse);
        })
    }

    create(course: Course): Observable<Course> {
        const copy = this.convert(course);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(course: Course): Observable<Course> {
        const copy = this.convert(course);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Course> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Course.
     */
    private convertItemFromServer(json: any): Course {
        const entity: Course = Object.assign(new Course(), json);
        return entity;
    }

    /**
     * Convert a Course to a JSON which can be sent to the server.
     */
    private convert(course: Course): Course {
        const copy: Course = Object.assign({}, course);
        return copy;
    }

    /***
     * Convert json data to format required by timetable
     */

    private convertTimetableData(data): object {
        const monday = this.addHoursToSlots(data, 0, 17, 0) // data.reduce(({ number }) => number > 0 && number < 17)
        const tuesday = this.addHoursToSlots(data, 16, 33, 1) // data.reduce(({ number }) => number > 16 && number < 33)
        const wednesday = this.addHoursToSlots(data, 32, 49, 2) // data.reduce(({ number }) => number > 32 && number < 49)
        const thursday = this.addHoursToSlots(data, 48, 65, 3) // data.reduce(({ number }) => number > 48 && number < 65)
        const friday = this.addHoursToSlots(data, 64, 81, 4) // data.reduce(({ number }) => number > 64 && number < 81)
        return {
            friday,
            monday,
            tuesday,
            wednesday,
            thursday,
        }

    }

    private addHoursToSlots(data, fromSlot, toSlot, dayNumber): SubjectSlot[] {
        return data
            .filter(({ number }) => number > fromSlot && number < toSlot)
            .map((item) => {
                const { number, subject: { duration } } = item;
                const baseTime = moment().minute(0).hour(8);
                const fromDayBegining = 45 * (number - 1 - (dayNumber * 16));
                const startTime = baseTime.add(fromDayBegining , 'm').format('HH[:]mm');
                const endTime = baseTime.add((45), 'm').format('HH[:]mm');

                return Object.assign(item, { startTime }, { endTime });
            })
    }
}
