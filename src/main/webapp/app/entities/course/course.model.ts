import { BaseEntity } from './../../shared';

export class Course implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public semester?: number,
        public subjects?: BaseEntity[],
        public students?: BaseEntity[],
        public departmentId?: number,
    ) {
    }
}
