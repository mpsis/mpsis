import { BaseEntity } from './../../shared';

export class Lecturer implements BaseEntity {
    constructor(
        public id?: number,
        public subjects?: BaseEntity[],
    ) {
    }
}
