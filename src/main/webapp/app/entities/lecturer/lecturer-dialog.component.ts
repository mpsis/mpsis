import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Lecturer } from './lecturer.model';
import { LecturerPopupService } from './lecturer-popup.service';
import { LecturerService } from './lecturer.service';

@Component({
    selector: 'jhi-lecturer-dialog',
    templateUrl: './lecturer-dialog.component.html'
})
export class LecturerDialogComponent implements OnInit {

    lecturer: Lecturer;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private lecturerService: LecturerService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.lecturer.id !== undefined) {
            this.subscribeToSaveResponse(
                this.lecturerService.update(this.lecturer));
        } else {
            this.subscribeToSaveResponse(
                this.lecturerService.create(this.lecturer));
        }
    }

    private subscribeToSaveResponse(result: Observable<Lecturer>) {
        result.subscribe((res: Lecturer) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Lecturer) {
        this.eventManager.broadcast({ name: 'lecturerListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-lecturer-popup',
    template: ''
})
export class LecturerPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private lecturerPopupService: LecturerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.lecturerPopupService
                    .open(LecturerDialogComponent as Component, params['id']);
            } else {
                this.lecturerPopupService
                    .open(LecturerDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
