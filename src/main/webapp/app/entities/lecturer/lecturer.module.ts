import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpsisSharedModule } from '../../shared';
import {
    LecturerService,
    LecturerPopupService,
    LecturerComponent,
    LecturerDetailComponent,
    LecturerDialogComponent,
    LecturerPopupComponent,
    LecturerDeletePopupComponent,
    LecturerDeleteDialogComponent,
    lecturerRoute,
    lecturerPopupRoute,
    LecturerResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...lecturerRoute,
    ...lecturerPopupRoute,
];

@NgModule({
    imports: [
        MpsisSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        LecturerComponent,
        LecturerDetailComponent,
        LecturerDialogComponent,
        LecturerDeleteDialogComponent,
        LecturerPopupComponent,
        LecturerDeletePopupComponent,
    ],
    entryComponents: [
        LecturerComponent,
        LecturerDialogComponent,
        LecturerPopupComponent,
        LecturerDeleteDialogComponent,
        LecturerDeletePopupComponent,
    ],
    providers: [
        LecturerService,
        LecturerPopupService,
        LecturerResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpsisLecturerModule {}
