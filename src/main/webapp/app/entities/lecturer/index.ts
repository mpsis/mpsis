export * from './lecturer.model';
export * from './lecturer-popup.service';
export * from './lecturer.service';
export * from './lecturer-dialog.component';
export * from './lecturer-delete-dialog.component';
export * from './lecturer-detail.component';
export * from './lecturer.component';
export * from './lecturer.route';
