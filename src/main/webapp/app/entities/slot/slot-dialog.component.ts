import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Slot } from './slot.model';
import { SlotPopupService } from './slot-popup.service';
import { SlotService } from './slot.service';
import { Room, RoomService } from '../room';
import { SubjectResource, SubjectResourceService } from '../subject-resource';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-slot-dialog',
    templateUrl: './slot-dialog.component.html'
})
export class SlotDialogComponent implements OnInit {

    slot: Slot;
    isSaving: boolean;

    rooms: Room[];

    subjectresources: SubjectResource[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private slotService: SlotService,
        private roomService: RoomService,
        private subjectResourceService: SubjectResourceService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.roomService.query()
            .subscribe((res: ResponseWrapper) => { this.rooms = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.subjectResourceService.query()
            .subscribe((res: ResponseWrapper) => { this.subjectresources = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.slot.id !== undefined) {
            this.subscribeToSaveResponse(
                this.slotService.update(this.slot));
        } else {
            this.subscribeToSaveResponse(
                this.slotService.create(this.slot));
        }
    }

    private subscribeToSaveResponse(result: Observable<Slot>) {
        result.subscribe((res: Slot) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Slot) {
        this.eventManager.broadcast({ name: 'slotListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRoomById(index: number, item: Room) {
        return item.id;
    }

    trackSubjectResourceById(index: number, item: SubjectResource) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-slot-popup',
    template: ''
})
export class SlotPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private slotPopupService: SlotPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.slotPopupService
                    .open(SlotDialogComponent as Component, params['id']);
            } else {
                this.slotPopupService
                    .open(SlotDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
