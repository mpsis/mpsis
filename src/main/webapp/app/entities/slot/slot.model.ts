import { BaseEntity } from './../../shared';

export const enum Day {
    'MONDAY',
    'TUESDAY',
    'WEDNESDAY',
    'THURSDAY',
    'FRIDAY'
}

export class Slot implements BaseEntity {
    constructor(
        public id?: number,
        public day?: Day,
        public number?: number,
        public roomId?: number,
        public subjectId?: number,
    ) {
    }
}
