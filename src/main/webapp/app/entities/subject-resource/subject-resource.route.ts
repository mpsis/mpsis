import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SubjectResourceComponent } from './subject-resource.component';
import { SubjectResourceDetailComponent } from './subject-resource-detail.component';
import { SubjectResourcePopupComponent } from './subject-resource-dialog.component';
import { SubjectResourceDeletePopupComponent } from './subject-resource-delete-dialog.component';

export const subjectResourceRoute: Routes = [
    {
        path: 'subject-resource',
        component: SubjectResourceComponent,
        data: {
            authorities: ['ROLE_PRINCIPAL'],
            pageTitle: 'mpsisApp.subjectResource.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'subject-resource/:id',
        component: SubjectResourceDetailComponent,
        data: {
            authorities: ['ROLE_PRINCIPAL'],
            pageTitle: 'mpsisApp.subjectResource.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const subjectResourcePopupRoute: Routes = [
    {
        path: 'subject-resource-new',
        component: SubjectResourcePopupComponent,
        data: {
            authorities: ['ROLE_PRINCIPAL'],
            pageTitle: 'mpsisApp.subjectResource.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'subject-resource/:id/edit',
        component: SubjectResourcePopupComponent,
        data: {
            authorities: ['ROLE_PRINCIPAL'],
            pageTitle: 'mpsisApp.subjectResource.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'subject-resource/:id/delete',
        component: SubjectResourceDeletePopupComponent,
        data: {
            authorities: ['ROLE_PRINCIPAL'],
            pageTitle: 'mpsisApp.subjectResource.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
