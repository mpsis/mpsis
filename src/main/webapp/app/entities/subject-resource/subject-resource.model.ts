import { BaseEntity } from './../../shared';

export const enum SubjectType {
    'LECTURE',
    'LABOLATORY',
    'EXERCISE'
}

export class SubjectResource implements BaseEntity {
    constructor(
        public id?: number,
        public duration?: number,
        public type?: SubjectType,
        public slots?: BaseEntity[],
        public subjectId?: number,
    ) {
    }
}
