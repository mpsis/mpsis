import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SubjectResource } from './subject-resource.model';
import { SubjectResourcePopupService } from './subject-resource-popup.service';
import { SubjectResourceService } from './subject-resource.service';

@Component({
    selector: 'jhi-subject-resource-delete-dialog',
    templateUrl: './subject-resource-delete-dialog.component.html'
})
export class SubjectResourceDeleteDialogComponent {

    subjectResource: SubjectResource;

    constructor(
        private subjectResourceService: SubjectResourceService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.subjectResourceService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'subjectResourceListModification',
                content: 'Deleted an subjectResource'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-subject-resource-delete-popup',
    template: ''
})
export class SubjectResourceDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private subjectResourcePopupService: SubjectResourcePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.subjectResourcePopupService
                .open(SubjectResourceDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
