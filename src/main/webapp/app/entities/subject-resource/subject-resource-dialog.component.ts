import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SubjectResource } from './subject-resource.model';
import { SubjectResourcePopupService } from './subject-resource-popup.service';
import { SubjectResourceService } from './subject-resource.service';
import { Subject, SubjectService } from '../subject';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-subject-resource-dialog',
    templateUrl: './subject-resource-dialog.component.html'
})
export class SubjectResourceDialogComponent implements OnInit {

    subjectResource: SubjectResource;
    isSaving: boolean;

    subjects: Subject[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private subjectResourceService: SubjectResourceService,
        private subjectService: SubjectService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.subjectService.query()
            .subscribe((res: ResponseWrapper) => { this.subjects = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.subjectResource.id !== undefined) {
            this.subscribeToSaveResponse(
                this.subjectResourceService.update(this.subjectResource));
        } else {
            this.subscribeToSaveResponse(
                this.subjectResourceService.create(this.subjectResource));
        }
    }

    private subscribeToSaveResponse(result: Observable<SubjectResource>) {
        result.subscribe((res: SubjectResource) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: SubjectResource) {
        this.eventManager.broadcast({ name: 'subjectResourceListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackSubjectById(index: number, item: Subject) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-subject-resource-popup',
    template: ''
})
export class SubjectResourcePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private subjectResourcePopupService: SubjectResourcePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.subjectResourcePopupService
                    .open(SubjectResourceDialogComponent as Component, params['id']);
            } else {
                this.subjectResourcePopupService
                    .open(SubjectResourceDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
