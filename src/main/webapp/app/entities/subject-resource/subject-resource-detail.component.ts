import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { SubjectResource } from './subject-resource.model';
import { SubjectResourceService } from './subject-resource.service';

@Component({
    selector: 'jhi-subject-resource-detail',
    templateUrl: './subject-resource-detail.component.html'
})
export class SubjectResourceDetailComponent implements OnInit, OnDestroy {

    subjectResource: SubjectResource;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private subjectResourceService: SubjectResourceService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSubjectResources();
    }

    load(id) {
        this.subjectResourceService.find(id).subscribe((subjectResource) => {
            this.subjectResource = subjectResource;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSubjectResources() {
        this.eventSubscriber = this.eventManager.subscribe(
            'subjectResourceListModification',
            (response) => this.load(this.subjectResource.id)
        );
    }
}
