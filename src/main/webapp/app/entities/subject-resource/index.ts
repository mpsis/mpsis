export * from './subject-resource.model';
export * from './subject-resource-popup.service';
export * from './subject-resource.service';
export * from './subject-resource-dialog.component';
export * from './subject-resource-delete-dialog.component';
export * from './subject-resource-detail.component';
export * from './subject-resource.component';
export * from './subject-resource.route';
