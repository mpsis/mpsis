import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpsisSharedModule } from '../../shared';
import {
    SubjectResourceService,
    SubjectResourcePopupService,
    SubjectResourceComponent,
    SubjectResourceDetailComponent,
    SubjectResourceDialogComponent,
    SubjectResourcePopupComponent,
    SubjectResourceDeletePopupComponent,
    SubjectResourceDeleteDialogComponent,
    subjectResourceRoute,
    subjectResourcePopupRoute,
} from './';

const ENTITY_STATES = [
    ...subjectResourceRoute,
    ...subjectResourcePopupRoute,
];

@NgModule({
    imports: [
        MpsisSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        SubjectResourceComponent,
        SubjectResourceDetailComponent,
        SubjectResourceDialogComponent,
        SubjectResourceDeleteDialogComponent,
        SubjectResourcePopupComponent,
        SubjectResourceDeletePopupComponent,
    ],
    entryComponents: [
        SubjectResourceComponent,
        SubjectResourceDialogComponent,
        SubjectResourcePopupComponent,
        SubjectResourceDeleteDialogComponent,
        SubjectResourceDeletePopupComponent,
    ],
    providers: [
        SubjectResourceService,
        SubjectResourcePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpsisSubjectResourceModule {}
