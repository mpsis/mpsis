import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { MpsisSlotModule } from './slot/slot.module';
import { MpsisBuildingModule } from './building/building.module';
import { MpsisRoomModule } from './room/room.module';
import { MpsisDepartmentModule } from './department/department.module';
import { MpsisCourseModule } from './course/course.module';
import { MpsisSubjectModule } from './subject/subject.module';
import { MpsisSubjectResourceModule } from './subject-resource/subject-resource.module';
import { MpsisLecturerModule } from './lecturer/lecturer.module';
import { MpsisStudentModule } from './student/student.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        MpsisSlotModule,
        MpsisBuildingModule,
        MpsisRoomModule,
        MpsisDepartmentModule,
        MpsisCourseModule,
        MpsisSubjectModule,
        MpsisSubjectResourceModule,
        MpsisLecturerModule,
        MpsisStudentModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpsisEntityModule {}
