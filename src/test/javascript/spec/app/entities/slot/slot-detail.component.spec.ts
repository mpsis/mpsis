/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { MpsisTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { SlotDetailComponent } from '../../../../../../main/webapp/app/entities/slot/slot-detail.component';
import { SlotService } from '../../../../../../main/webapp/app/entities/slot/slot.service';
import { Slot } from '../../../../../../main/webapp/app/entities/slot/slot.model';

describe('Component Tests', () => {

    describe('Slot Management Detail Component', () => {
        let comp: SlotDetailComponent;
        let fixture: ComponentFixture<SlotDetailComponent>;
        let service: SlotService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [MpsisTestModule],
                declarations: [SlotDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    SlotService,
                    JhiEventManager
                ]
            }).overrideTemplate(SlotDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SlotDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SlotService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Slot(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.slot).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
