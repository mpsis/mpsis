/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { MpsisTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { SubjectResourceDetailComponent } from '../../../../../../main/webapp/app/entities/subject-resource/subject-resource-detail.component';
import { SubjectResourceService } from '../../../../../../main/webapp/app/entities/subject-resource/subject-resource.service';
import { SubjectResource } from '../../../../../../main/webapp/app/entities/subject-resource/subject-resource.model';

describe('Component Tests', () => {

    describe('SubjectResource Management Detail Component', () => {
        let comp: SubjectResourceDetailComponent;
        let fixture: ComponentFixture<SubjectResourceDetailComponent>;
        let service: SubjectResourceService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [MpsisTestModule],
                declarations: [SubjectResourceDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    SubjectResourceService,
                    JhiEventManager
                ]
            }).overrideTemplate(SubjectResourceDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SubjectResourceDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubjectResourceService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new SubjectResource(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.subjectResource).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
