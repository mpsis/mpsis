/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { MpsisTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { LecturerDetailComponent } from '../../../../../../main/webapp/app/entities/lecturer/lecturer-detail.component';
import { LecturerService } from '../../../../../../main/webapp/app/entities/lecturer/lecturer.service';
import { Lecturer } from '../../../../../../main/webapp/app/entities/lecturer/lecturer.model';

describe('Component Tests', () => {

    describe('Lecturer Management Detail Component', () => {
        let comp: LecturerDetailComponent;
        let fixture: ComponentFixture<LecturerDetailComponent>;
        let service: LecturerService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [MpsisTestModule],
                declarations: [LecturerDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    LecturerService,
                    JhiEventManager
                ]
            }).overrideTemplate(LecturerDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LecturerDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LecturerService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Lecturer(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.lecturer).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
