package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.MpsisApp;

import com.mycompany.myapp.domain.SubjectResource;
import com.mycompany.myapp.repository.SubjectResourceRepository;
import com.mycompany.myapp.service.SubjectResourceService;
import com.mycompany.myapp.repository.search.SubjectResourceSearchRepository;
import com.mycompany.myapp.service.dto.SubjectResourceDTO;
import com.mycompany.myapp.service.mapper.SubjectResourceMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.domain.enumeration.SubjectType;
/**
 * Test class for the SubjectResourceResource REST controller.
 *
 * @see SubjectResourceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MpsisApp.class)
public class SubjectResourceResourceIntTest {

    private static final Integer DEFAULT_DURATION = 1;
    private static final Integer UPDATED_DURATION = 2;

    private static final SubjectType DEFAULT_TYPE = SubjectType.LECTURE;
    private static final SubjectType UPDATED_TYPE = SubjectType.LABOLATORY;

    @Autowired
    private SubjectResourceRepository subjectResourceRepository;

    @Autowired
    private SubjectResourceMapper subjectResourceMapper;

    @Autowired
    private SubjectResourceService subjectResourceService;

    @Autowired
    private SubjectResourceSearchRepository subjectResourceSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSubjectResourceMockMvc;

    private SubjectResource subjectResource;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SubjectResourceResource subjectResourceResource = new SubjectResourceResource(subjectResourceService);
        this.restSubjectResourceMockMvc = MockMvcBuilders.standaloneSetup(subjectResourceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubjectResource createEntity(EntityManager em) {
        SubjectResource subjectResource = new SubjectResource()
            .duration(DEFAULT_DURATION)
            .type(DEFAULT_TYPE);
        return subjectResource;
    }

    @Before
    public void initTest() {
        subjectResourceSearchRepository.deleteAll();
        subjectResource = createEntity(em);
    }

    @Test
    @Transactional
    public void createSubjectResource() throws Exception {
        int databaseSizeBeforeCreate = subjectResourceRepository.findAll().size();

        // Create the SubjectResource
        SubjectResourceDTO subjectResourceDTO = subjectResourceMapper.toDto(subjectResource);
        restSubjectResourceMockMvc.perform(post("/api/subject-resources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subjectResourceDTO)))
            .andExpect(status().isCreated());

        // Validate the SubjectResource in the database
        List<SubjectResource> subjectResourceList = subjectResourceRepository.findAll();
        assertThat(subjectResourceList).hasSize(databaseSizeBeforeCreate + 1);
        SubjectResource testSubjectResource = subjectResourceList.get(subjectResourceList.size() - 1);
        assertThat(testSubjectResource.getDuration()).isEqualTo(DEFAULT_DURATION);
        assertThat(testSubjectResource.getType()).isEqualTo(DEFAULT_TYPE);

        // Validate the SubjectResource in Elasticsearch
        SubjectResource subjectResourceEs = subjectResourceSearchRepository.findOne(testSubjectResource.getId());
        assertThat(subjectResourceEs).isEqualToComparingFieldByField(testSubjectResource);
    }

    @Test
    @Transactional
    public void createSubjectResourceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = subjectResourceRepository.findAll().size();

        // Create the SubjectResource with an existing ID
        subjectResource.setId(1L);
        SubjectResourceDTO subjectResourceDTO = subjectResourceMapper.toDto(subjectResource);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubjectResourceMockMvc.perform(post("/api/subject-resources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subjectResourceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SubjectResource in the database
        List<SubjectResource> subjectResourceList = subjectResourceRepository.findAll();
        assertThat(subjectResourceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDurationIsRequired() throws Exception {
        int databaseSizeBeforeTest = subjectResourceRepository.findAll().size();
        // set the field null
        subjectResource.setDuration(null);

        // Create the SubjectResource, which fails.
        SubjectResourceDTO subjectResourceDTO = subjectResourceMapper.toDto(subjectResource);

        restSubjectResourceMockMvc.perform(post("/api/subject-resources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subjectResourceDTO)))
            .andExpect(status().isBadRequest());

        List<SubjectResource> subjectResourceList = subjectResourceRepository.findAll();
        assertThat(subjectResourceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = subjectResourceRepository.findAll().size();
        // set the field null
        subjectResource.setType(null);

        // Create the SubjectResource, which fails.
        SubjectResourceDTO subjectResourceDTO = subjectResourceMapper.toDto(subjectResource);

        restSubjectResourceMockMvc.perform(post("/api/subject-resources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subjectResourceDTO)))
            .andExpect(status().isBadRequest());

        List<SubjectResource> subjectResourceList = subjectResourceRepository.findAll();
        assertThat(subjectResourceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSubjectResources() throws Exception {
        // Initialize the database
        subjectResourceRepository.saveAndFlush(subjectResource);

        // Get all the subjectResourceList
        restSubjectResourceMockMvc.perform(get("/api/subject-resources?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subjectResource.getId().intValue())))
            .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getSubjectResource() throws Exception {
        // Initialize the database
        subjectResourceRepository.saveAndFlush(subjectResource);

        // Get the subjectResource
        restSubjectResourceMockMvc.perform(get("/api/subject-resources/{id}", subjectResource.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(subjectResource.getId().intValue()))
            .andExpect(jsonPath("$.duration").value(DEFAULT_DURATION))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSubjectResource() throws Exception {
        // Get the subjectResource
        restSubjectResourceMockMvc.perform(get("/api/subject-resources/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSubjectResource() throws Exception {
        // Initialize the database
        subjectResourceRepository.saveAndFlush(subjectResource);
        subjectResourceSearchRepository.save(subjectResource);
        int databaseSizeBeforeUpdate = subjectResourceRepository.findAll().size();

        // Update the subjectResource
        SubjectResource updatedSubjectResource = subjectResourceRepository.findOne(subjectResource.getId());
        updatedSubjectResource
            .duration(UPDATED_DURATION)
            .type(UPDATED_TYPE);
        SubjectResourceDTO subjectResourceDTO = subjectResourceMapper.toDto(updatedSubjectResource);

        restSubjectResourceMockMvc.perform(put("/api/subject-resources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subjectResourceDTO)))
            .andExpect(status().isOk());

        // Validate the SubjectResource in the database
        List<SubjectResource> subjectResourceList = subjectResourceRepository.findAll();
        assertThat(subjectResourceList).hasSize(databaseSizeBeforeUpdate);
        SubjectResource testSubjectResource = subjectResourceList.get(subjectResourceList.size() - 1);
        assertThat(testSubjectResource.getDuration()).isEqualTo(UPDATED_DURATION);
        assertThat(testSubjectResource.getType()).isEqualTo(UPDATED_TYPE);

        // Validate the SubjectResource in Elasticsearch
        SubjectResource subjectResourceEs = subjectResourceSearchRepository.findOne(testSubjectResource.getId());
        assertThat(subjectResourceEs).isEqualToComparingFieldByField(testSubjectResource);
    }

    @Test
    @Transactional
    public void updateNonExistingSubjectResource() throws Exception {
        int databaseSizeBeforeUpdate = subjectResourceRepository.findAll().size();

        // Create the SubjectResource
        SubjectResourceDTO subjectResourceDTO = subjectResourceMapper.toDto(subjectResource);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSubjectResourceMockMvc.perform(put("/api/subject-resources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subjectResourceDTO)))
            .andExpect(status().isCreated());

        // Validate the SubjectResource in the database
        List<SubjectResource> subjectResourceList = subjectResourceRepository.findAll();
        assertThat(subjectResourceList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSubjectResource() throws Exception {
        // Initialize the database
        subjectResourceRepository.saveAndFlush(subjectResource);
        subjectResourceSearchRepository.save(subjectResource);
        int databaseSizeBeforeDelete = subjectResourceRepository.findAll().size();

        // Get the subjectResource
        restSubjectResourceMockMvc.perform(delete("/api/subject-resources/{id}", subjectResource.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean subjectResourceExistsInEs = subjectResourceSearchRepository.exists(subjectResource.getId());
        assertThat(subjectResourceExistsInEs).isFalse();

        // Validate the database is empty
        List<SubjectResource> subjectResourceList = subjectResourceRepository.findAll();
        assertThat(subjectResourceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSubjectResource() throws Exception {
        // Initialize the database
        subjectResourceRepository.saveAndFlush(subjectResource);
        subjectResourceSearchRepository.save(subjectResource);

        // Search the subjectResource
        restSubjectResourceMockMvc.perform(get("/api/_search/subject-resources?query=id:" + subjectResource.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subjectResource.getId().intValue())))
            .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubjectResource.class);
        SubjectResource subjectResource1 = new SubjectResource();
        subjectResource1.setId(1L);
        SubjectResource subjectResource2 = new SubjectResource();
        subjectResource2.setId(subjectResource1.getId());
        assertThat(subjectResource1).isEqualTo(subjectResource2);
        subjectResource2.setId(2L);
        assertThat(subjectResource1).isNotEqualTo(subjectResource2);
        subjectResource1.setId(null);
        assertThat(subjectResource1).isNotEqualTo(subjectResource2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubjectResourceDTO.class);
        SubjectResourceDTO subjectResourceDTO1 = new SubjectResourceDTO();
        subjectResourceDTO1.setId(1L);
        SubjectResourceDTO subjectResourceDTO2 = new SubjectResourceDTO();
        assertThat(subjectResourceDTO1).isNotEqualTo(subjectResourceDTO2);
        subjectResourceDTO2.setId(subjectResourceDTO1.getId());
        assertThat(subjectResourceDTO1).isEqualTo(subjectResourceDTO2);
        subjectResourceDTO2.setId(2L);
        assertThat(subjectResourceDTO1).isNotEqualTo(subjectResourceDTO2);
        subjectResourceDTO1.setId(null);
        assertThat(subjectResourceDTO1).isNotEqualTo(subjectResourceDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(subjectResourceMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(subjectResourceMapper.fromId(null)).isNull();
    }
}
